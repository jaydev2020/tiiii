import { Ifo } from './types'

const ifos: Ifo[] = [
  {
    id: 'IFO',
    address: '0x2cc26dd730F548dc4ac291ae7D84a0C96980d2cB',
    isActive: true,
    name: 'Long Swap Token',
    subTitle: 'LongSwap - The Best Modern Yield Farm on Binance Smart Chain',
    description:
      'LongSwap - The Best Modern Yield Farm on Binance Smart Chain.',
    launchDate: 'February. 05',
    launchTime: '8AM UTC',
    saleAmount: '1,000,000 XXX',
    raiseAmount: '$000,000',
    pizzaToBurn: '$000,000',
    projectSiteUrl: 'https://longswap.org',
    currency: 'PIZZA-BNB LP',
    currencyAddress: '0x2cc26dd730F548dc4ac291ae7D84a0C96980d2cB',
    tokenDecimals: 18,
    releaseBlockNumber: 4086064,
  },
]

export default ifos
